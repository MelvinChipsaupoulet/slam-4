﻿using System;
using System.Collections.Generic;
using SYNAPSINFO;

namespace sysnapseConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ///*Sauvegarde d'un intervenant*/
            //Intervenant unIntervenant = new Intervenant();
            //unIntervenant.Nom = "Billy";
            //unIntervenant.TauxHoraire = 55;
            //unIntervenant.Save();

            ///* Modif d'un intervenant */
            //unIntervenant.TauxHoraire = 312;
            //unIntervenant.Save();

            ///* Sauvegarde d'une mission */
            //Mission uneMission = new Mission();
            //uneMission.Nom = "Impossible";
            //uneMission.Description = "Vraiment impossible ?";
            //uneMission.NbHeuresPrevues = 5;
            //uneMission.Intervenant = unIntervenant;
            //uneMission.Save();

            /////* Mofid de l'intervenant d'une mission */
            //Intervenant unAutreIntervenant = Intervenant.Fetch(31);
            //uneMission.Intervenant = unAutreIntervenant;
            //uneMission.ReleveHoraire.Add(new TimeSpan(1537129194), 58);
            //uneMission.Save();

            //Mission uneAutreMission = Mission.Fetch(1);

            //Console.ReadLine();

            Projet p1 = new Projet();
            p1.NomProjet = "Dev Appli-Frais";
            p1.DateDebutProjet = DateTime.Now.AddMonths(1);
            p1.DateFinProjet = DateTime.Now.AddMonths(8);
            p1.PrixFacture = 125;
            p1.Save();

            Projet p2 = new Projet();
            p2.NomProjet = "Dev Appli-Tholdi";
            p2.DateDebutProjet = DateTime.Now.AddMonths(1);
            p2.DateFinProjet = DateTime.Now.AddMonths(5);
            p2.PrixFacture = 254;
            p2.Save();

            p2.NomProjet = "DEV THOLDI";
            p2.Save();

            Projet p3 = Projet.Fetch(1); //attention à vérifier l’identifiant en base de données
            List<Projet> listeProjets = Projet.FetchAll();

            foreach (Projet Projetlol in listeProjets)
            {
                Console.WriteLine("id du projet {0} | nombres de mission(s) {1}", Projetlol.Id, Projetlol.Missions.Count);
                Console.WriteLine("{0}|{1}|{2}|{3}", Projetlol.NomProjet, Projetlol.DateDebutProjet, Projetlol.DateFinProjet, Projetlol.PrixFacture);
                Console.WriteLine("");
            }
            Console.ReadLine();
            Console.ReadKey();
        }
    }
}
