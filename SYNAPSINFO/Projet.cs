﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace SYNAPSINFO
{
    public class Projet
    {
        public int Id { get; private set; } // Primary Key (Bd)
        public DateTime DateDebutProjet { get; set; } 
        public DateTime DateFinProjet { get; set; } 

        public decimal PrixFacture { get; set; } 
        public string NomProjet { get; set; } 
        public List<Mission> Missions { get; set; } // Foreign Key (Bd)

        public Projet()
        {
            Id = -1; //Identifie un contact non référencé dans la base de données
            DateDebutProjet = DateTime.Now; // lors de l'insertion utiliser unProjet.dateDebutProjet.Ticks
            DateFinProjet = DateDebutProjet.AddMonths(3);
            PrixFacture = 0;
            NomProjet = "Projet";
            Missions = new List<Mission>();
             
        }

        private decimal cumulCoutMO()
        {

            return 0;
        }
        
        public decimal margeBruteCourante()
        {

            return 0;
        }

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql =
            "SELECT id, dateDebutProjet, dateFinProjet, prixFacture, nomProjet FROM projet";

        private static string _selectByIdSql =
            "SELECT id, dateDebutProjet, dateFinProjet, prixFacture, nomProjet FROM projet WHERE id = ?id ";

        private static string _updateSql =
            "UPDATE projet SET dateDebutProjet = ?dateDebutProjet, dateFinProjet=?dateFinProjet , prixFacture=?prixFacture, nomProjet=?nomProjet  WHERE id=?id ";

        private static string _insertSql =
            "INSERT INTO projet (dateDebutProjet,dateFinProjet,prixFacture,nomProjet) VALUES (?dateDebutProjet,?dateFinProjet,?prixFacture,?nomProjet)";

        private static string _deleteByIdSql =
            "DELETE FROM projet WHERE id = ?id";

        private static string _getLastInsertId =
            "SELECT id FROM projet WHERE dateDebutProjet = ?dateDebutProjet AND dateFinProjet=?dateFinProjet AND prixFacture=?prixFacture AND nomProjet = ?nomProjet   ";

        private static string _getByIdProjet =
            "SELECT mission.* FROM mission, projet WHERE projet.id = mission.idProjet AND idProjet = ?idProjet";
        #endregion

        #region Méthodes d'accès aux données
        /// <summary>
        /// Valorise un objet projet depuis le système de gestion de bases de données
        /// </summary>
        /// <param name="id">La valeur de la clé primaire</param>
        public static Projet Fetch(int idProjet)
        {
            Projet unProjet = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", idProjet));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            bool existEnregistrement = jeuEnregistrements.Read();
            if (existEnregistrement)
            {
                unProjet = new Projet();
                unProjet.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());
                unProjet.DateDebutProjet = new DateTime(Convert.ToInt64(jeuEnregistrements["dateDebutProjet"].ToString()));
                unProjet.DateFinProjet = new DateTime(Convert.ToInt64(jeuEnregistrements["dateFinProjet"].ToString()));
                unProjet.PrixFacture = Convert.ToDecimal(jeuEnregistrements["prixFacture"].ToString());
                unProjet.NomProjet = jeuEnregistrements["nomProjet"].ToString();
                unProjet.Missions = Projet.GetMissions(unProjet);
            }
            openConnection.Close();
            return unProjet;
        }

        /// <summary>
        /// Sauvegarde ou met à jour un Projet dans la base de données
        /// </summary>
        public void Save()
        {
            if (Id == -1)
            {
                Insert();
            }
            else
            {
                Update();

            }
            SaveMission();
        }

        private void SaveMission()
        {
            foreach (Mission uneMission in Missions)
            {
                uneMission.Save();
            }
        }

        /// <summary>
        /// Supprime le projet représenté par l'instance courante dans le SGBD
        /// </summary>
        public void Delete()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            Id = -1;
            openConnection.Close();
        }

        private void Update()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Parameters.Add(new MySqlParameter("?dateDebutProjet", DateDebutProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?dateFinProjet", DateFinProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?prixFacture", PrixFacture));
            commandSql.Parameters.Add(new MySqlParameter("?nomProjet", NomProjet));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();

        }

        /// <summary>
        /// Insertion des données dans un Projet
        /// <summary>

        private void Insert()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(new MySqlParameter("?dateDebutProjet", DateDebutProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?dateFinProjet", DateFinProjet.Ticks));
            commandSql.Parameters.Add(new MySqlParameter("?prixFacture", PrixFacture));
            commandSql.Parameters.Add(new MySqlParameter("?nomProjet", NomProjet));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            Id = Convert.ToInt16(commandSql.LastInsertedId);
            openConnection.Close();
        }

        /// <summary>
        /// Retourne une collection contenant les projets
        /// </summary>
        /// <returns>Une collection de projets</returns>
        public static List<Projet> FetchAll()
        {
            List<Projet> resultat = new List<Projet>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Projet unProjet = new Projet();
                unProjet.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());
                unProjet.DateDebutProjet = new DateTime(Convert.ToInt64(jeuEnregistrements["dateDebutProjet"].ToString()));
                unProjet.DateFinProjet = new DateTime(Convert.ToInt64(jeuEnregistrements["dateFinProjet"].ToString()));
                string prixFacture = jeuEnregistrements["prixFacture"].ToString();
                unProjet.PrixFacture = Convert.ToDecimal(prixFacture);
                unProjet.NomProjet = Convert.ToString(jeuEnregistrements["nomProjet"].ToString());
                unProjet.Missions = Projet.GetMissions(unProjet);
                resultat.Add(unProjet);
            }
            openConnection.Close();
            return resultat;
        }

        #endregion

        #region Méthodes du TD

        // Ajoute une mission donnée à la collection de mission d'un projet
        public void AjouterMissions(Mission uneMission)
        {
            int i=0;
            foreach (Mission missionCourante in Missions)
	        {
                if (missionCourante.Equals(uneMission))
	            {
                    i++;
	            }
	        }
            if (i == 0)
            {
                Missions.Add(uneMission);
            }

        }

        // Supprime une mission donnée de la collection de mission d'un projet
        public void SupprimerMission(Mission uneMission)
        {
            foreach (Mission missionCourante in Missions)
            {
                
                if (missionCourante.Equals(uneMission))
                {
                    Missions.Remove(uneMission);  
                    break;
                }
                
            }
        }

        private static List<Mission> GetMissions(Projet unProjet)
        {
            List<Mission> Missions = new List<Mission>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _getByIdProjet;
            commandSql.Parameters.Add(new MySqlParameter("?idProjet", unProjet.Id));
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Mission uneMission = Mission.Fetch(Convert.ToInt16(jeuEnregistrements["id"].ToString()));
                Missions.Add(uneMission);
            }
            openConnection.Close();
            return Missions;
        }



        #endregion 
    }
}
