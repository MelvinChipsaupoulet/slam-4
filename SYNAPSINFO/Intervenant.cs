﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace SYNAPSINFO
{
    public class Intervenant
    {
        public int IdIntervenant;
        public string Nom;
        public decimal TauxHoraire;

        public decimal tauxHoraire
        {
            get
            {
                return TauxHoraire;
            }
        }

        public MySqlDbType Id { get; internal set; }

        public Intervenant()
        {
            IdIntervenant = -1;
            Nom = "";
            TauxHoraire = 0;
        }

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données

        private static string _selectSql = "SELECT id, nom, tauxHoraire FROM intervenant";

        private static string _selectByIdSql = "SELECT id, nom, tauxHoraire FROM intervenant WHERE id = ?id";

        private static string _updateSql = "UPDATE intervenant SET nom=?nom , tauxHoraire=?tauxHoraire WHERE id=?id";

        private static string _insertSql = "INSERT INTO intervenant (nom, tauxHoraire) VALUES (?nom, ?tauxHoraire)";

        private static string _deleteByIdSql = "DELETE FROM intervenant WHERE id=?id";

        private static string _getLastInsertId = "SELECT idIntervenant FROM intervenant WHERE nom=?nom AND tauxHoraire=?tauxHoraire";

        #endregion

        #region Méthodes d'accès aux données

        public static Intervenant Fetch(int idIntervenant)
        {
            Intervenant i = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand(); // Initialisation d'un objet qui permet d'interroger la base de donnée.
            commandSql.CommandText = _selectByIdSql; // Définit la requête à utiliser.
            commandSql.Parameters.Add(new MySqlParameter("?id", idIntervenant)); //Transmet en paramètre à utiliser lors de l'envoi de la requête.
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();

            bool existeEnregistrement = jeuEnregistrements.Read(); // Lecture de l'enregistrement.
            if (existeEnregistrement)
            {
                i = new Intervenant();
                i.IdIntervenant = Convert.ToInt16(jeuEnregistrements["id"].ToString());
                i.Nom = (jeuEnregistrements["nom"].ToString());
                i.TauxHoraire = Convert.ToDecimal(jeuEnregistrements["tauxHoraire"].ToString());
            }
            openConnection.Close();
            return i;
        }

        public void Save()
        {
            if (IdIntervenant == -1)
            {
                Insert();
            }

            else
            {
                Update();
            }
        }

        public void Delete()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?IdIntervenant", IdIntervenant));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            IdIntervenant = -1;
        }

        public void Update()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(new MySqlParameter("?IdIntervenant", IdIntervenant));
            commandSql.Parameters.Add(new MySqlParameter("?Nom", Nom));
            commandSql.Parameters.Add(new MySqlParameter("?TauxHoraire", TauxHoraire));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();
        }

        public void Insert()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertSql;
            commandSql.Parameters.Add(new MySqlParameter("?IdIntervenant", IdIntervenant));
            commandSql.Parameters.Add(new MySqlParameter("?Nom", Nom));
            commandSql.Parameters.Add(new MySqlParameter("?TauxHoraire", TauxHoraire));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            MySqlCommand commandGetLastId = openConnection.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandGetLastId.Parameters.Add(new MySqlParameter("?IdIntervenant", IdIntervenant));
            commandGetLastId.Parameters.Add(new MySqlParameter("?Nom", Nom));
            commandGetLastId.Parameters.Add(new MySqlParameter("?TauxHoraire", TauxHoraire));
            commandGetLastId.Prepare();
            MySqlDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();

            bool existEnregistrement = jeuEnregistrements.Read();
            if(existEnregistrement)
            {
                IdIntervenant = Convert.ToInt16(jeuEnregistrements["idIntervenant"].ToString());
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            openConnection.Close();
        }

        public static List<Intervenant> FetchAll()
        {
            List<Intervenant> resultat = new List<Intervenant>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Intervenant unIntervenant = new Intervenant();
                string idIntervenant = jeuEnregistrements["idIntervenant"].ToString();
                unIntervenant.IdIntervenant = Convert.ToInt16(idIntervenant);
                unIntervenant.Nom = jeuEnregistrements["nom"].ToString();
                string tauxHoraire = jeuEnregistrements["tauxHoraire"].ToString();
                unIntervenant.TauxHoraire = Convert.ToDecimal(tauxHoraire);
                resultat.Add(unIntervenant);
            }
            openConnection.Close();
            return resultat;
        }

        //public decimal getTauxHoraire()
        //{

        //    return 0;
        //}
    }
}
#endregion