﻿using System;
using System.Collections.Generic;
using SYNAPSINFO;

namespace SYNAPSINFO
{
    class Program
    {
        static void Main(string[] args)
        {
            ///*Sauvegarde d'un intervenant*/
            //Intervenant unIntervenant = new Intervenant();
            //unIntervenant.Nom = "Billy";
            //unIntervenant.TauxHoraire = 55;
            //unIntervenant.Save();

            ///* Modif d'un intervenant */
            //unIntervenant.TauxHoraire = 312;
            //unIntervenant.Save();

            ///* Sauvegarde d'une mission */
            //Mission uneMission = new Mission();
            //uneMission.Nom = "Impossible";
            //uneMission.Description = "Vraiment impossible ?";
            //uneMission.NbHeuresPrevues = 5;
            //uneMission.Intervenant = unIntervenant;
            //uneMission.Save();

            /////* Mofid de l'intervenant d'une mission */
            //Intervenant unAutreIntervenant = Intervenant.Fetch(31);
            //uneMission.Intervenant = unAutreIntervenant;
            //uneMission.ReleveHoraire.Add(new TimeSpan(1537129194), 58);
            //uneMission.Save();

            //Mission uneAutreMission = Mission.Fetch(1);

            //Console.ReadLine();

            Projet p1 = new Projet();
            p1._nomProjet = "Dev Appli-Frais";
            p1._dateDebutProjet = DateTime.Now.AddMonths(1);
            p1._dateFinProjet = DateTime.Now.AddMonths(8);
            p1._prixFacture = 125;
            p1.Save();

            Projet p2 = new Projet();
            p2._nomProjet = "Dev Appli-Tholdi";
            p2._dateDebutProjet = DateTime.Now.AddMonths(1);
            p2._dateFinProjet = DateTime.Now.AddMonths(5);
            p2._prixFacture = 254;
            p2.Save();

            p2._nomProjet = "DEV THOLDI";
            p2.Save();

            Projet p3 = Projet.Fetch(1); //attention à vérifier l’identifiant en base de données
            List<Projet> listeProjets = Projet.FetchAll();

        }
    }
}
